var colors = require('colors');
const people = require('./people');
const { PrintAge } = require('./trinker');
const tr = require('./trinker');

console.log(tr.title());
console.log("Model des données : ");
console.log(people[0]);
console.log(tr.line('LEVEL 1'));
console.log("Nombre d'hommes : ",                                                                   tr.nbOfMale(people));
console.log("Nombre de femmes : ",                                                                  tr.nbOfFemale(people));
console.log("Nombre de personnes qui cherchent un homme :",                                         tr.nbOfMaleInterest(people));
console.log("Nombre de personnes qui cherchent une femme :",                                        tr.nbOfFemaleInterest(people));
console.log("Nombre de personnes qui gagnent plus de 2000$ :",                                      tr.nbOf_pers_income(people,2000) );
console.log("Nombre de personnes qui aiment les Drama :",                                           tr.nbOf_pref_movie(people,'Drama') );
console.log("Nombre de femmes qui aiment la science-fiction :",                                     tr.nbOf_pref_movie(tr.allFemale(people),'Sci-Fi') );
console.log(tr.line('LEVEL 2'));                                                                    //nbOf_pref_movie
console.log("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ :",          tr.nbOf_pers_income_interest(people,1482,'Documentary') );
console.log("Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ :",      tr.print_pers_income(people,4000) );
console.log("Homme le plus riche (nom et id) :",                                                    tr.IncomeMax_pers(tr.allMale(people)) );
console.log("Salaire moyen :",                                                                      tr.IncomeAverage_p(people) );
console.log("Salaire médian :",                                                                     tr.IncomeMedian_p(people) );
console.log("Nombre de personnes qui habitent dans l'hémisphère nord :",                            tr.nbOf_latitude_NS(people,'N') );
console.log("Salaire moyen des personnes qui habitent dans l'hémisphère sud :",                     tr.IncomeAverage_p_ids(people,tr.Ids_Of_latitude_NS(people,'S') ) );
console.log(tr.line('LEVEL 3'));            
console.log("Personne qui habite le plus près de Bérénice Cawt (nom et id) :",                      tr.Nearest_Of_Dist_Name(people,'Bérénice','Cawt') );
console.log("Personne qui habite le plus près de Ruì Brach (nom et id) :",                          tr.Nearest_Of_Dist_Name(people,'Ruì','Brach') );
console.log("les 10 personnes qui habite les plus près de Josée Boshard (nom et id) :",             tr.Nb_Nearest_Of_Dist_Name(people,'Josée','Boshard',10) );
console.log("Les noms et ids des 23 personnes qui travaillent chez google :",                       tr.PartOf_email(people,'google') );
console.log("Personne la plus agée :",                                                              tr.PrintOldest(people) );
console.log("Personne la plus jeune :",                                                             tr.PrintYoungest(people) );
console.log("Moyenne des différences d'age :",                                                      tr.DiffAge_Moyenne(people) );
console.log(tr.line('LEVEL 4'));            
console.log("Genre de film le plus populaire :",                                                    tr.Print_MovieBest(people) );
console.log("Genres de film par ordre de popularité :",                                             tr.Print_MovieClassement(people) );
console.log("Liste des genres de film et nombre de personnes qui les préfèrent :",                  tr.Print_MovieClassement(people) );
console.log("Age moyen des hommes qui aiment les films noirs :",                                    tr.Age_Of_GenderMovie(people,'Male','Film-Noir') );
console.log(`Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire 
de Paris et gagnent moins que la moyenne des hommes :`,                                             tr.Print_Age_F_FilmNoir_SalInf(people,0.5,'F','Film-Noir') );
console.log(`Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une 
préférence de film en commun (afficher les deux et la distance entre les deux):`,                   tr.Print_Nearest_HH_SameMovie(people,'M','M') );
console.log("Liste des couples femmes / hommes qui ont les même préférences de films :",            tr.Print_Tab_G1G2_SameMovie(people,'F','M') );
console.log(tr.line('MATCH'));
/* 
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui sont les plus proches.
    Puis ceux qui ont le plus de goût en commun.
    Pour les couples hétéroséxuel on s'assure que la femme gagne au moins 10% de moins que l'homme mais plus de la moitié.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi les films d'aventure.
    Le différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agée des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.   ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction   ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum   ߷    ߷    ߷    
*/
console.log("liste de couples à matcher (nom et id pour chaque membre du couple) :",             tr.match(people));