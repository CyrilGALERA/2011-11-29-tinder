const { slice } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    // !!! LEVEL 1 ======================================================================================
    // Nb Hommes Femmes
    // Salaires
    // Movies
    //
    allMale: function(p){
        let Tab_Male=[];
        for( pers of p) {
            if(pers.gender === 'Male') {
                // Tab_Male[pers.id]=pers;
                // Pour rester bien indexer... mais comble de toute facon avec index 0 1 undefined...
                Tab_Male.push(pers);
            }
        }
        return Tab_Male;
    },
    allFemale: function(p){
        let Tab_Female=[];
        for( pers of p) {
            if(pers.gender === 'Female') {
                Tab_Female.push(pers);
            }
        }
        return Tab_Female;
    },
    nbOfMale: function(p){
        return this.allMale(p).length;
    },
    nbOfFemale: function(p){
        // return p.length - this.nbOfMale(p);
        // ou
        return this.allFemale(p).length;
    },
    nbOfMaleInterest: function(p){
        let n=0;
        for( pers of p) {
            if (pers.looking_for === 'M') {n++;}
        }
        return n;
    },
    nbOfFemaleInterest: function(p){
        let n=0;
        for( pers of p) {
            if (pers.looking_for === 'F') {n++;}
        }
        return n;
    },
    //
    // ==== Income =================================================================== Income ====
    //
    income_pers: function(pers) {
        return parseFloat(pers.income.slice(1));
    },
    income_test: function(pers,income) {
        if (this.income_pers(pers)  >= income) {
            return 1;} else {  return 0;
        }
    },
    nbOf_pers_income: function(p,income){
        let n=0;
        for( pers of p) {
            n += this.income_test(pers, income);   
        }
        return n;
    },
    //
    // ==== Movie =================================================================== Movie ====
    //
    pref_movie_test: function(pers,Movie_Style) {
        if (pers.pref_movie.includes(Movie_Style) ) {
            return 1;} else {return 0;
        }
    },
    nbOf_pref_movie: function(p,Movie_Style){
        let n=0;
        for( pers of p) {      
            n += this.pref_movie_test(pers, Movie_Style);   
        }
        return n;
    },
    //
    // !!! LEVEL 2 ======================================================================================
    //
    // Salaire Moyen Median Income
    // Hemisphère N S
    //
    nbOf_pers_income_interest: function(p,income,Movie_Style){
        let n=0;
        for( pers of p) {      
            if( this.pref_movie_test(pers, Movie_Style) && this.income_test(pers, income) ) {n++}
        }
        return n;
    },
    print_pers_4id: function(pers) {
        return ""+pers.last_name+" "+pers.first_name+" id:"+pers.id+" "+pers.income+"";
    },
    print_pers_income: function(p, income) {
        let n=0;
        for( pers of p) {     
            if( this.income_test(pers, income) ) {
                n++;
                this.print_pers_4id(pers);
            }
        }
        return n;
    },
    IncomeMax_pers: function(p) {
        let income_max=0;
        let richest_pers= p[0];
        for( pers of p) {  
            let income_pers = this.income_pers(pers);
            if( income_max < income_pers ) {
                income_max = income_pers;
                richest_pers = pers;
            }
        }
        // console.log(richest_pers.last_name," ", richest_pers.first_name," id ",richest_pers.id," est la personne la plus riche! ",richest_pers.income);
        return richest_pers;
    },
    IncomeAverage_p: function(p) {
        let income_sum=0;
        for( pers of p) {  
            income_sum += this.income_pers(pers);
        }
        return income_sum/p.length;
    },
    IncomeAverage_p_ids: function(p,ids) {
        let income_sum=0;
        for( i of ids) {
            // console.log( p[i-1] );
            income_sum += this.income_pers(p[i-1]);
        }
        return income_sum/ids.length;
    },
    IncomeMedian_p: function(p) {
        let incomes=[];
        for( pers of p) {
            incomes.push( this.income_pers(pers) );
        }
        incomes.sort((a, b) => a - b);

        let length_div2 = Math.floor(incomes.length/2);
        let remainder_div2 = p.length % 2;
        if( remainder_div2 != 0 ) {
            return incomes[ length_div2+1 ];
        } else {
            let income_p49 = incomes[ length_div2];
            let income_p51 = incomes[ length_div2+1 ];
            return (income_p49+income_p51)/2;
        }
    },
    //
    // ==== Hemisphere N S ============================================= Hemisphere N S ====
    //
    latitude_N_test: function(pers) {
        if(pers.latitude>0) {
            return 1;
        } else {
            return 0;
        }
    },
    latitude_S_test: function(pers) {
            if(pers.latitude<=0) {
                return 1;
            } else {
                return 0;
            }
    },
    nbOf_latitude_NS: function(p,NS) {
        return this.AllOf_latitude_NS(p,NS).length;
    },
    AllOf_latitude_NS: function(p,NS) {
        let n=0;
        let Tab_pers=[];
        if( NS === 'N') {
            for( pers of p) {    
                if( pers.latitude>0) Tab_pers.push( pers );
            }
        } else if( NS === 'S') {
            for( pers of p) {  
                if( pers.latitude<=0) Tab_pers.push( pers );    
            }
        }
        return Tab_pers;
    },
    Ids_Of_latitude_NS: function(p,NS) {
        let n=0;
        let Tab_ids=[];
        if( NS === 'N') {
            for( pers of p) {    
                if( pers.latitude>0) Tab_ids.push( pers.id );
            }
        } else if( NS === 'S') {
            for( pers of p) {  
                if( pers.latitude<=0) Tab_ids.push( pers.id );    
            }
        }
        return Tab_ids;
    },
    //
    // !!! LEVEL 3 ===================================================================================== !!! LEVEL 3 
    // Proximité Distance
    // Job Email
    // Age Date
    //
    // ==== Proximité Distance ============================================ Job Email ====
    //
    dist_2pers: function(pers1,pers2) {
        let dx = pers1.longitude - pers2.longitude;
        let dy = pers1.latitude - pers2.latitude;
        return Math.sqrt( (dx*dx + dy*dy) );
    },
    // Return Nearest[0 1]: [0]=pers_nearest [1]=dist(pers-pers_nearest)
    Nearest_Of_Dist_Pers: function(p, pers) { 
        let dist_nearest = 10000000;
        let pers_nearest = undefined;

        for( pers_i of p) {
            if( pers_i.id !== pers.id ) {
                let dist_i = this.dist_2pers(pers,pers_i)
                if( dist_i < dist_nearest ) {
                    pers_nearest = pers_i;
                    dist_nearest = dist_i;
                }
            }
        }
        // let p_d[0] = pers_nearest;
        // p_d[1] = dist_nearest;
        // let p_d = [pers_nearest, dist_nearest];
        return [pers_nearest, dist_nearest];
    },
    Find_PersName(p,first_name,last_name) {
        let pers_name=undefined;
        // let p_N = p.find( x => ( x.first_name=== first_name ));
        // !!!!
        // p_N[]. find filter ...
        // map...
        console.log(p_N);
        for( pers_i of p) {
            if( pers_i.first_name === first_name  &&   pers_i.last_name === last_name ) {
                pers_name = pers_i;
                break;
            }
        }
        return pers_name;
    },
    Nearest_Of_Dist_Name: function(p,first_name,last_name) {
        let pers_name = this.Find_PersName(p,first_name,last_name);
        let reponse="";
        if( pers_name ) { // trouvée !
            let nearest = this.Nearest_Of_Dist_Pers(p,pers_name);
            if( nearest ) {  // trouvée aussi !
                // console.log("La pers la plus proche est à ",nearest[1],", c est ", nearest[0].first_name, " ", nearest[0].last_name, " id: ",nearest[0].id);
                // reponse="La pers la plus proche est à ", nearest[1], " , c est ", nearest[0].first_name, " ", nearest[0].last_name, " id: ",nearest[0].id,"";
                reponse=" ( "+ nearest[0].last_name + " id: " + nearest[0].id + " à " + nearest[1] + " km )";
                // return reponse; //Non! uniquement à la fin!
            } else {
                reponse="Aucune pers trouvée... ! Problème...";
            }
        }
        return reponse;
    },

    Nb_Nearest_Of_Dist_Name: function(p,first_name,last_name,nb) {
        
        let pers_name = this.Find_PersName(p,first_name,last_name);
        let reponse="";

        if( pers_name ) { // trouvée !
            // Compute all the dist [pers_i, pers_name] (Object PersDist)
            // and store it in Tab_PersDist
            let Tab_PersDist=[];
            for( pers_i of p) { 
                let PersDist = {
                    pers:   pers_i, 
                    dist:   this.dist_2pers(pers_name,pers_i)
                };
                Tab_PersDist.push( PersDist );
            }
            // Sort the tab
            Tab_PersDist.sort( (pd1, pd2)  =>  (pd1.dist - pd2.dist) );

            // Print the nb nearest pers
            for(let i=1; i<=nb; i++) {
                reponse=reponse+
                        "\n ( " + Tab_PersDist[i].pers.last_name+" id: " + Tab_PersDist[i].pers.id + " dist = " + Tab_PersDist[i].dist + " )";
            }
        } else {
            reponse=""+first_name+" "+last_name+" n'a pas été trouvé(e)... Bizarre...";
        }

        return reponse;
    },
    //
    // ==== Job Email ================================================================ Job Email ====
    //
    PartOf_email: function(p,email_part) {
        let reponse="";
        for( let pers_i of p) {
            if( pers_i.email.includes(email_part) ) {
                reponse=reponse+
                        "\n ( " + pers_i.last_name + " id: " + pers_i.id + " )";
            }
        } 
        return reponse;
    },
    //
    // ==== Age ================================================================ Age ====
    //
    PersBirthday: function(p) {
        let p_age=p;
        let p_sort=p;
        let p_sort_ab=p;
        p_sort.sort();
        // p_sort_ab.sort(a,b);
        let p2 = p_sort_ab.sort( (a,b) => a.last_name - b.last_name );

        for(i=0; i<20; i++) {
            // classement direct de p en fonct des noms? des ages?...
            console.log("p_sort [",i,"] : id=", p_sort[i].id);
            console.log("p_sort_ab [",i,"] : id=", p2[i].id);

        }
    },
    YoungestOldest_Of: function(p) {
        let date_mini="2200-12-31";
        let date_maxi="1900-01-01";
        let pers_youngest, pers_oldest;
        for( let pers_i of p) {
            let date_i=pers_i.date_of_birth;
            if( date_i < date_mini ) { 
                pers_oldest = pers_i;
                date_mini = date_i;
            } else if( date_i > date_maxi) {
                pers_youngest = pers_i;
                date_maxi = date_i;
            }
        }
        return [ pers_youngest, pers_oldest];
    },
    PrintOldest: function(p) {
        let pers_oldest = this.YoungestOldest_Of(p)[1];
        let reponse=""+pers_oldest.first_name+" "+pers_oldest.last_name+" "+pers_oldest.date_of_birth+" ";
        // console.log(reponse);
        return reponse;
    },
    PrintYoungest: function(p) {
        let pers_youngest = this.YoungestOldest_Of(p)[0];
        let reponse=""+pers_youngest.first_name+" "+pers_youngest.last_name+" "+pers_youngest.date_of_birth+" ";
        // console.log(reponse);
        return reponse;
    },
    //
    Pers_YYYY_MM_DD2: function(pers) {
            let date_p = pers.date_of_birth;
            let year = parseInt( date_p.slice(0,4) ,10 );
            let month = parseInt( date_p.slice(5,7) ,10 );
            let day = parseInt( date_p.slice(8,10) ,10 );
            // console.log(year," ",month," ",day);
            return [year, month, day];
    },
    Today_YYYY_MM_DD2: function() {
        // let date = Date.now();
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() +1;
        let day = date.getDay();  
        // console.log(year," ",month," ",day);
        let ymd = [year, month, day];  
        return  ymd;
    },
    Pers_Age_YMD: function(pers) {
        let ymd = this.Today_YYYY_MM_DD2();
        let ymd_pers = this.Pers_YYYY_MM_DD2(pers);
        let ymd_age = [ ymd[0]-ymd_pers[0],
                        ymd[1]-ymd_pers[1],
                        ymd[2]-ymd_pers[2]
                    ];
        return ymd_age;   
    },
    Pers_Age_YY: function(pers) {
        let dob = pers.date_of_birth;
        // const result = dob.split("-").join(", ");
      
        let age = new Date(dob);
        // return ~~((Date.now() - age) / 31557600000);
        // Le nombre magique : 31557600000 est 24 * 3600 * 365.25 * 1000 Ce qui est la durée d'une année
        return ((Date.now() - age) / 31557600000);
    },
    DiffAge_YMD_p1p2: function(p1,p2) {
        let ymd_p1 = this.Pers_YYYY_MM_DD2(p1);
        let ymd_p2 = this.Pers_YYYY_MM_DD2(p2);
        let diff_age = [    ymd_p1[0]-ymd_p2[0],
                            ymd_p1[1]-ymd_p2[1],
                            ymd_p1[2]-ymd_p2[2]
                    ];
        // Oui mais attention a des diff d age negative !!!!!!!!!!!!!!!!!!!
        return diff_age;   
    },
    Age_Moyen: function(p) {
        let yyyy = mm = dd =0;
        for(pers_i of p) {
            let ymd_i = this.Pers_Age_YMD(pers_i);
            yyyy += ymd_i[0];
            mm   += ymd_i[1];
            dd   += ymd_i[2];
        }
        yyyy /= p.length;
        mm   /= p.length;
        dd   /= p.length;
        return [ yyyy, mm, dd];
    },
    DiffAge_Moyenne: function(p) {  
        let age_i, age_j, diff_age;
        let sum_diff = nb_diff = 0;

        let index=j=0;
        while( index <= p.length-2 ) {
            age_i = this.Pers_Age_YY( p[index] );
            index++;

            // Calcul la diff age entre p[index] et tous les p[j] suivant index+1 à length
            for( j=index; j<=p.length-1; j++) {
                age_j = this.Pers_Age_YY( p[j] );
                diff_age = Math.abs(age_i-age_j);
                sum_diff += diff_age;
                nb_diff ++;
            }
        // On recommence avec p[index++] et tous les p[j] suivant
        // ...
        // jusqu à p[length-2] et p[j=length-1]
        }
        return sum_diff / nb_diff;
    },
    PrintAge: function(p) {
        //
        let reponse = ""+this.DiffAge_Moyenne(p)+". La moyenne d age est "+this.Pers_Age_YY(pers)+"";
        return reponse;
    },
    //
    // ======================================================================================================
    //
    Tab_Movie: function() {
        return ['Adventure','Comedy','Documentary','Drama','Horror','Sci-Fi'];
    },
    Tab_Movie_People: function(p) {
        let movies = this.Tab_Movie();
        let tab_MoviePeople = [ [] ];
        // Compute the People for each Movies[i] as a tab list
        // tab_MoviePeople[0] = [0][pi, pj, p...]
        //
        // 1) But first, initialize all tab_MoviePeople[i] = []
        for( i=0; i<movies.length; i++ ) {
            tab_MoviePeople[i]=[];
        }

        // 2) Compute the People for each Movies[i] as a tab list
        for( pers of p) {
            let pref_movies = pers.pref_movie;
            for( i=0; i<movies.length; i++ ) {
                let movie_i = movies[i];
                if( pref_movies.includes(movie_i) ) {
                    tab_MoviePeople[i].push(pers);
                    // if tab_M... hasn't been initialize, it can't push in an undefine tab[i]
                }
            }
        }
        return tab_MoviePeople;
    },
    Tab_Movie_People2: function(p) {
        let Movies = this.Tab_Movie();
        let tab_MoviePeople = [];
        // Compute the People for each Movies[i] as a tab
        // tab_MoviePeople[0] = [pi, pj, p...]
        for( pers of p) {
            let pref_movies = pers.pref_movie;
            for( i=0; i<Movies.length; i++ ) {
                let movie_i = Movies[i];
                if( pref_movies.includes(movie_i) ) {
                    tab_MoviePeople[i].push(pers);
                }
            }
        }

        // Build Object { Movie -id -name People -list -nb}
        for( i=0; i<Movies.length; i++  ) {
            let Movie_People = {
                "movie_id":     i,
                "movie_name":   Movies[i],
                "pers_list":    tab_MoviePeople[i],
                "nb_pers":      tab_MoviePeople[i].length
            }
        // push...
        }
        return tab_MoviePeople;
    },
    Tab_Movie_NbPers: function(p) {
        let tab_MovieNb = [];

        // Ask tab Movies-People
        let tab_MoviePeople = this.Tab_Movie_People(p);

        // Compute for each Movie: People.length
        for( i=0; i<tab_MoviePeople.length; i++  ) {
            let Movie_NbPers = {
                "movie_id":     i,
                "nb_pers":      tab_MoviePeople[i].length
            }
            tab_MovieNb.push( Movie_NbPers );
        }

        // Sort tab People - Nb_People
        // Par ordre nb_pers décroissant: du + populaire au moins populaire
        // (m1,m2) => m2-m1 !!!
        tab_MovieNb.sort( (m1, m2) => (m2.nb_pers - m1.nb_pers) );

        return tab_MovieNb;
    },
    Print_MovieBest: function(p) {
        let movies = this.Tab_Movie();
        let tab_MovieNb = this.Tab_Movie_NbPers(p);

        let tmn_best = tab_MovieNb[ 0 ];
        let reponse = ""+movies[ tmn_best.movie_id ]+" avec "+tmn_best.nb_pers+" personnes";
        return reponse;
    },
    Print_MovieClassement: function(p) {
        let movies = this.Tab_Movie();
        let tab_MovieNb = this.Tab_Movie_NbPers(p);

        let reponse = "";
        for( let tmn of tab_MovieNb ) {
            reponse += "\n ( "+movies[ tmn.movie_id ]+" avec "+tmn.nb_pers+" personnes )";
        }
        return reponse;
    },

    // ======================================================================================================
    match: function(p){
        return "not implemented".red;
    }
}